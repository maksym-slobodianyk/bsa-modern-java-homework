package com.binary_studio.tree_max_depth;

import java.util.Arrays;
import java.util.List;
import java.util.Stack;

public final class DepartmentMaxDepth {

	private DepartmentMaxDepth() {
	}

	public static Integer calculateMaxDepth(Department rootDepartment) {
		if (rootDepartment == null) {
			return 0;
		}
		DepartmentWrapper target = findDeepestDepartment(rootDepartment);
		return target.getLayer() + 1;
	}

	/**
	 * Method explore all departments of the tree using depth-first search algorithm and
	 * find (one of) the deepest one
	 * @param root - root department
	 * @return - the deepest department combined with its layer in the tree
	 */
	private static DepartmentWrapper findDeepestDepartment(Department root) {
		Stack<DepartmentWrapper> descendants = new Stack<>();
		DepartmentWrapper res = new DepartmentWrapper(root, 0);
		descendants.push(res);
		while (!descendants.empty()) {
			DepartmentWrapper current = descendants.pop();
			if (current.isLeaf() && current.compareTo(res) == 1) {
				res = current;
			}
			else {
				current.getSubDepartments().stream().filter(d -> d != null)
						.forEach(d -> descendants.push(new DepartmentWrapper(d, current.getLayer() + 1)));
			}

		}
		return res;
	}

	/**
	 * Method print path from root department to target department
	 * @param root
	 * @param target
	 */
	private static void printPathTo(Department root, DepartmentWrapper target) {
		System.out.print("Path length: " + (target.getLayer() + 1) + "\t");
		findPathTo(root, target).stream().forEach(a -> {
			System.out.print((target.getDepartment().equals(a)) ? a.name + "\n" : a.name + " -- ");
		});
	}

	/**
	 * Method find path from root department to target department using depth-first search
	 * algorithm
	 * @param root - root department
	 * @param target - target department
	 * @return - list
	 */
	private static List<Department> findPathTo(Department root, DepartmentWrapper target) {
		Department[] path = new Department[target.getLayer() + 1];
		Stack<DepartmentWrapper> descendants = new Stack<>();
		DepartmentWrapper res = new DepartmentWrapper(root, 0);
		descendants.push(res);
		while (!descendants.empty()) {
			DepartmentWrapper current = descendants.pop();
			if (current.compareTo(target) == 0) {
				path[current.getLayer()] = current.getDepartment();
				return Arrays.asList(path);
			}
			else {
				current.getSubDepartments().stream().filter(d -> d != null)
						.forEach(d -> descendants.push(new DepartmentWrapper(d, current.getLayer() + 1)));
				path[current.getLayer()] = current.getDepartment();
			}

		}
		return Arrays.asList(path);
	}

}
