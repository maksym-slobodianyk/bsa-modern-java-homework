package com.binary_studio.tree_max_depth;

import java.util.List;

public class DepartmentWrapper implements Comparable<DepartmentWrapper> {

	private Department department;

	private int layer;

	public DepartmentWrapper(Department department, int layer) {
		this.department = department;
		this.layer = layer;
	}

	public Department getDepartment() {
		return this.department;
	}

	public List<Department> getSubDepartments() {
		return this.department.subDepartments;
	}

	public int getLayer() {
		return this.layer;
	}

	/**
	 *
	 * @return - true if department is leaf of department tree
	 */
	public boolean isLeaf() {
		// TODO: analyze if this condition is realistic
		if (this.department == null) {
			return false;
		}
		if (this.department.subDepartments.size() == 0) {
			return true;
		}
		return this.department.subDepartments.stream().filter(a -> a != null).count() == 0 ? true : false;
	}

	@Override
	public int compareTo(DepartmentWrapper departmentWrapper) {
		if (this.layer > departmentWrapper.layer) {
			return 1;
		}
		if (this.layer < departmentWrapper.layer) {
			return -1;
		}
		return 0;
	}

}
