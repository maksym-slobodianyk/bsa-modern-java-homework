package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class DefenciveSubsystemImpl implements DefenciveSubsystem {

	private final String name;

	private final Integer capacitorUsage;

	private final Integer pgRequirement;

	private final Integer impactReduction;

	private final Integer shieldRegen;

	private final Integer hullRegen;

	private DefenciveSubsystemImpl(String name, PositiveInteger powergridConsumption,
			PositiveInteger capacitorConsumption, PositiveInteger impactReductionPercent,
			PositiveInteger shieldRegeneration, PositiveInteger hullRegeneration) {
		this.name = name;

		this.pgRequirement = powergridConsumption.value();

		this.capacitorUsage = capacitorConsumption.value();

		this.impactReduction = impactReductionPercent.value();

		this.shieldRegen = shieldRegeneration.value();

		this.hullRegen = hullRegeneration.value();
	}

	public static DefenciveSubsystemImpl construct(String name, PositiveInteger powergridConsumption,
			PositiveInteger capacitorConsumption, PositiveInteger impactReductionPercent,
			PositiveInteger shieldRegeneration, PositiveInteger hullRegeneration) throws IllegalArgumentException {
		if (name == null || name.isBlank()) {
			throw new IllegalArgumentException("Name should be not null and not empty");
		}
		return new DefenciveSubsystemImpl(name, powergridConsumption, capacitorConsumption, impactReductionPercent,
				shieldRegeneration, hullRegeneration);
	}

	@Override
	public PositiveInteger getPowerGridConsumption() {
		return PositiveInteger.of(this.pgRequirement);
	}

	@Override
	public PositiveInteger getCapacitorConsumption() {
		return PositiveInteger.of(this.capacitorUsage);
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public AttackAction reduceDamage(AttackAction incomingDamage) {
		double initialDamage = incomingDamage.damage.value();
		double reduction = (initialDamage * this.impactReduction) / 100;
		reduction = (reduction > initialDamage * 0.95) ? initialDamage * 0.95 : reduction; // Normalization
		return new AttackAction(PositiveInteger.ofDouble(initialDamage - reduction), incomingDamage.attacker,
				incomingDamage.target, incomingDamage.weapon);
	}

	@Override
	public RegenerateAction regenerate() {
		return new RegenerateAction(PositiveInteger.of(this.shieldRegen), PositiveInteger.of(this.hullRegen));
	}

}
