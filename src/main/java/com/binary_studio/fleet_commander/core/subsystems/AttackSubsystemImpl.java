package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;

public final class AttackSubsystemImpl implements AttackSubsystem {

	private final String name;

	private final Integer powergridRequirments;

	private final Integer capacitorConsumption;

	private final Integer optimalSpeed;

	private final Integer optimalSize;

	private final Integer baseDamage;

	private AttackSubsystemImpl(String name, PositiveInteger powergridRequirments, PositiveInteger capacitorConsumption,
			PositiveInteger optimalSpeed, PositiveInteger optimalSize, PositiveInteger baseDamage) {
		this.name = name;
		this.powergridRequirments = powergridRequirments.value();
		this.capacitorConsumption = capacitorConsumption.value();
		this.optimalSpeed = optimalSpeed.value();
		this.optimalSize = optimalSize.value();
		this.baseDamage = baseDamage.value();
	}

	public static AttackSubsystemImpl construct(String name, PositiveInteger powergridRequirments,
			PositiveInteger capacitorConsumption, PositiveInteger optimalSpeed, PositiveInteger optimalSize,
			PositiveInteger baseDamage) throws IllegalArgumentException {
		if (name == null || name.isBlank()) {
			throw new IllegalArgumentException("Name should be not null and not empty");
		}
		return new AttackSubsystemImpl(name, powergridRequirments, capacitorConsumption, optimalSpeed, optimalSize,
				baseDamage);
	}

	@Override
	public PositiveInteger getPowerGridConsumption() {
		return PositiveInteger.of(this.powergridRequirments);
	}

	@Override
	public PositiveInteger getCapacitorConsumption() {
		return PositiveInteger.of(this.capacitorConsumption);
	}

	@Override
	public PositiveInteger attack(Attackable target) {
		double sizeReductionModifier = (target.getSize().value() >= this.optimalSize) ? 1
				: (Double.valueOf(target.getSize().value()) / this.optimalSize);
		double speedReductionModifier = (target.getCurrentSpeed().value() <= this.optimalSpeed) ? 1
				: (Double.valueOf(this.optimalSpeed) / (2 * target.getCurrentSpeed().value()));
		double damage = this.baseDamage * Math.min(sizeReductionModifier, speedReductionModifier);
		return PositiveInteger.ofDouble(damage);
	}

	@Override
	public String getName() {
		return this.name;
	}

}
