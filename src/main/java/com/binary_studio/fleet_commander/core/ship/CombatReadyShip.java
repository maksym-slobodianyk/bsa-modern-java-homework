package com.binary_studio.fleet_commander.core.ship;

import java.util.Optional;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.AttackResult;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.ship.contract.CombatReadyVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class CombatReadyShip implements CombatReadyVessel {

	private Integer shieldHP;

	private Integer hullHP;

	private Integer capacitorAmount;

	private final String name;

	private final Integer initShieldHP;

	private final Integer initHullHP;

	private final Integer initCapacitorAmount;

	private final Integer powergridOutput;

	private final Integer speed;

	private final Integer size;

	private final Integer capacitorRechargeRate;

	private final AttackSubsystem attackSubsystem;

	private final DefenciveSubsystem defenciveSubsystem;

	public CombatReadyShip(DockedShip dockedShip) {
		this.name = dockedShip.getName();
		this.shieldHP = dockedShip.getShieldHP();
		this.hullHP = dockedShip.getHullHP();
		this.initShieldHP = dockedShip.getShieldHP();
		this.initHullHP = dockedShip.getHullHP();
		this.powergridOutput = dockedShip.getPowergridOutput();
		this.capacitorAmount = dockedShip.getCapacitorAmount();
		this.speed = dockedShip.getSpeed();
		this.size = dockedShip.getSize();
		this.initCapacitorAmount = dockedShip.getCapacitorAmount();
		this.capacitorRechargeRate = dockedShip.getCapacitorRechargeRate();
		this.attackSubsystem = dockedShip.getAttackSubsystem();
		this.defenciveSubsystem = dockedShip.getDefenciveSubsystem();
	}

	@Override
	public void endTurn() {
		this.capacitorAmount += (this.capacitorRechargeRate + this.capacitorAmount < this.initCapacitorAmount)
				? this.capacitorRechargeRate : this.initCapacitorAmount - this.capacitorAmount;
	}

	@Override
	public void startTurn() {
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public PositiveInteger getSize() {
		return PositiveInteger.of(this.size);
	}

	@Override
	public PositiveInteger getCurrentSpeed() {
		return PositiveInteger.of(this.speed);
	}

	@Override
	public Optional<AttackAction> attack(Attackable target) {
		if (this.capacitorAmount < this.attackSubsystem.getCapacitorConsumption().value()) {
			return Optional.empty();
		}
		this.capacitorAmount -= this.attackSubsystem.getCapacitorConsumption().value();
		return Optional.of(new AttackAction(this.attackSubsystem.attack(target), this, target, this.attackSubsystem));
	}

	@Override
	public AttackResult applyAttack(AttackAction attack) {
		int damage = this.defenciveSubsystem.reduceDamage(attack).damage.value();
		this.shieldHP -= damage;
		if (this.shieldHP < 0) {
			this.hullHP += this.shieldHP;
			this.shieldHP = 0;
		}
		if (this.hullHP > 0) {
			return new AttackResult.DamageRecived(attack.weapon, PositiveInteger.of(damage), attack.target);
		}
		return new AttackResult.Destroyed();
	}

	@Override
	public Optional<RegenerateAction> regenerate() {
		if (this.capacitorAmount < this.defenciveSubsystem.getCapacitorConsumption().value()) {
			return Optional.empty();
		}
		RegenerateAction defRegen = this.defenciveSubsystem.regenerate();
		int hullRegen = (defRegen.hullHPRegenerated.value() + this.hullHP < this.initHullHP)
				? defRegen.hullHPRegenerated.value() : this.initHullHP - this.hullHP;
		int shieldRegen = (defRegen.shieldHPRegenerated.value() + this.shieldHP < this.initShieldHP)
				? defRegen.shieldHPRegenerated.value() : this.initShieldHP - this.shieldHP;
		this.shieldHP += shieldRegen;
		this.hullHP += hullRegen;
		this.capacitorAmount -= this.defenciveSubsystem.getCapacitorConsumption().value();
		return Optional.of(new RegenerateAction(PositiveInteger.of(shieldRegen), PositiveInteger.of(hullRegen)));
	}

}
