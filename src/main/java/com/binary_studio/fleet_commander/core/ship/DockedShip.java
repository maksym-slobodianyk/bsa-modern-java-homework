package com.binary_studio.fleet_commander.core.ship;

import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.exceptions.InsufficientPowergridException;
import com.binary_studio.fleet_commander.core.exceptions.NotAllSubsystemsFitted;
import com.binary_studio.fleet_commander.core.ship.contract.ModularVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class DockedShip implements ModularVessel {

	private final String name;

	private final Integer shieldHP;

	private final Integer hullHP;

	private final Integer powergridOutput;

	private final Integer capacitorAmount;

	private final Integer capacitorRechargeRate;

	private final Integer speed;

	private final Integer size;

	private AttackSubsystem attackSubsystem;

	private DefenciveSubsystem defenciveSubsystem;

	private DockedShip(String name, PositiveInteger shieldHP, PositiveInteger hullHP, PositiveInteger powergridOutput,
			PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate, PositiveInteger speed,
			PositiveInteger size) {
		this.name = name;
		this.shieldHP = shieldHP.value();
		this.hullHP = hullHP.value();
		this.powergridOutput = powergridOutput.value();
		this.capacitorAmount = capacitorAmount.value();
		this.capacitorRechargeRate = capacitorRechargeRate.value();
		this.speed = speed.value();
		this.size = size.value();
	}

	public static DockedShip construct(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
			PositiveInteger powergridOutput, PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate,
			PositiveInteger speed, PositiveInteger size) {
		return new DockedShip(name, shieldHP, hullHP, powergridOutput, capacitorAmount, capacitorRechargeRate, speed,
				size);
	}

	@Override
	public void fitAttackSubsystem(AttackSubsystem subsystem) throws InsufficientPowergridException {
		if (this.defenciveSubsystem != null) {
			int freeEnergy = this.powergridOutput - this.defenciveSubsystem.getPowerGridConsumption().value();
			if (freeEnergy - subsystem.getPowerGridConsumption().value() < 0) {
				throw new InsufficientPowergridException(freeEnergy - subsystem.getPowerGridConsumption().value());
			}
		}
		if (subsystem != null && subsystem.getPowerGridConsumption().value() > this.powergridOutput) {
			throw new InsufficientPowergridException(
					this.powergridOutput - subsystem.getPowerGridConsumption().value());
		}
		this.attackSubsystem = subsystem;
	}

	@Override
	public void fitDefensiveSubsystem(DefenciveSubsystem subsystem) throws InsufficientPowergridException {
		if (this.attackSubsystem != null) {
			int freeEnergy = this.powergridOutput - this.attackSubsystem.getPowerGridConsumption().value();
			if (freeEnergy - subsystem.getPowerGridConsumption().value() < 0) {
				throw new InsufficientPowergridException(
						this.powergridOutput - subsystem.getPowerGridConsumption().value());
			}
		}
		if (subsystem != null && subsystem.getPowerGridConsumption().value() > this.powergridOutput) {
			throw new InsufficientPowergridException(
					this.powergridOutput - subsystem.getPowerGridConsumption().value());
		}
		this.defenciveSubsystem = subsystem;

	}

	public CombatReadyShip undock() throws NotAllSubsystemsFitted {
		if (this.attackSubsystem == null && this.defenciveSubsystem == null) {
			throw NotAllSubsystemsFitted.bothMissing();
		}
		if (this.attackSubsystem == null) {
			throw NotAllSubsystemsFitted.attackMissing();
		}
		if (this.defenciveSubsystem == null) {
			throw NotAllSubsystemsFitted.defenciveMissing();
		}
		return new CombatReadyShip(this);
	}

	public String getName() {
		return this.name;
	}

	public Integer getShieldHP() {
		return this.shieldHP;
	}

	public Integer getHullHP() {
		return this.hullHP;
	}

	public Integer getPowergridOutput() {
		return this.powergridOutput;
	}

	public Integer getCapacitorAmount() {
		return this.capacitorAmount;
	}

	public Integer getCapacitorRechargeRate() {
		return this.capacitorRechargeRate;
	}

	public AttackSubsystem getAttackSubsystem() {
		return this.attackSubsystem;
	}

	public DefenciveSubsystem getDefenciveSubsystem() {
		return this.defenciveSubsystem;
	}

	public Integer getSpeed() {
		return this.speed;
	}

	public Integer getSize() {
		return this.size;
	}

}
